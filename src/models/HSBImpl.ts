import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HSB, HSBModel, LAB, RGB, XYZ, YUV } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";

class HSBImpl extends AbstractColor<HSBModel> implements HSB {
  public static fromCMYK(model: CMYK): HSB {
    const rgb = RGBImpl.fromCMYK(model);

    return this.fromRGB(rgb);
  }

  public static fromHLS(model: HLS): HSB {
    const rgb = RGBImpl.fromHLS(model);

    return this.fromRGB(rgb);
  }

  public static fromLab(model: LAB): HSB {
    const rgb = RGBImpl.fromLab(model);

    return this.fromRGB(rgb);
  }

  public static fromRGB(model: RGB): HSB {
    const { R, G, B } = model.getColorRepresentation();

    const varR = R / 255;
    const varG = G / 255;
    const varB = B / 255;

    const varMin = Math.min(varR, varG, varB); //Min. value of RGB
    const varMax = Math.max(varR, varG, varB); //Max. value of RGB
    const delMax = varMax - varMin; //Delta RGB value

    let h = 0;
    let s = 0;
    let b = varMax;

    if (delMax !== 0) {
      s = delMax / varMax;

      const delR = ((varMax - varR) / 6 + delMax / 2) / delMax;
      const delG = ((varMax - varG) / 6 + delMax / 2) / delMax;
      const delB = ((varMax - varB) / 6 + delMax / 2) / delMax;

      if (varR === varMax) {
        h = delB - delG;
      } else if (varG === varMax) {
        h = 1 / 3 + delR - delB;
      } else if (varB === varMax) {
        h = 2 / 3 + delG - delR;
      }

      if (h < 0) {
        h += 1;
      }

      if (h > 1) {
        h -= 1;
      }
    }

    return new HSBImpl({ H: Math.floor(h * 360), S: Math.floor(s * 100), B: Math.floor(b * 100) });
  }

  public static fromXYZ(model: XYZ): HSB {
    const rgb = RGBImpl.fromXYZ(model);

    return this.fromRGB(rgb);
  }

  public static fromYUV(model: YUV): HSB {
    const rgb = RGBImpl.fromYUV(model);

    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: HSBModel) {
    super(colorRepresentation);
    this.typename = "HSB";
  }
}

export default HSBImpl;
