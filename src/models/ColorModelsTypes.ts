import type { IAbstractColor } from "./AbstractColor";

/**
 * RGB, CMYK, XYZ, HSB,  HLS, Lab, YUV
 */

export type RGBModel = {
  R: number;
  G: number;
  B: number;
};

export type HLSModel = {
  H: number;
  L: number;
  S: number;
};

export type XYZModel = {
  X: number;
  Y: number;
  Z: number;
};

export type YUVModel = {
  Y: number;
  U: number;
  V: number;
};

export type CMYKModel = {
  C: number;
  M: number;
  Y: number;
  K: number;
};

export type LABModel = {
  L: number;
  A: number;
  B: number;
};

export type HSBModel = {
  H: number;
  S: number;
  B: number;
};

export interface XYZ extends IAbstractColor<XYZModel> {}

export interface RGB extends IAbstractColor<RGBModel> {}

export interface HLS extends IAbstractColor<HLSModel> {}

export interface YUV extends IAbstractColor<YUVModel> {}

export interface CMYK extends IAbstractColor<CMYKModel> {}

export interface LAB extends IAbstractColor<LABModel> {}

export interface HSB extends IAbstractColor<HSBModel> {}
