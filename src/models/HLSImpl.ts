import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HLSModel, HSB, LAB, RGB, XYZ, YUV } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";

/**
 * Тон
 * Насыщенность
 * Яркость
 */
class HLSImpl extends AbstractColor<HLSModel> implements HLS {
  public static fromCMYK(model: CMYK): HLS {
    const rgb = RGBImpl.fromCMYK(model);

    return this.fromRGB(rgb);
  }

  public static fromHSB(model: HSB): HLS {
    const rgb = RGBImpl.fromHSB(model);

    return this.fromRGB(rgb);
  }

  public static fromLab(model: LAB): HLS {
    const rgb = RGBImpl.fromLab(model);

    return this.fromRGB(rgb);
  }

  public static fromRGB(model: RGB): HLS {
    const { R, G, B } = model.getColorRepresentation();

    let varR = R / 255;
    let varG = G / 255;
    let varB = B / 255;

    const varMin = Math.min(varR, varG, varB);
    const varMax = Math.max(varR, varG, varB);
    const delMax = varMax - varMin;

    let h = 0;
    let s = 0;
    let l = (varMax + varMin) / 2;

    if (delMax !== 0) {
      if (l < 0.5) {
        s = delMax / (varMax + varMin);
      } else {
        s = delMax / (2 - varMax - varMin);
      }

      const delR = ((varMax - varR) / 6 + delMax / 2) / delMax;
      const delG = ((varMax - varG) / 6 + delMax / 2) / delMax;
      const delB = ((varMax - varB) / 6 + delMax / 2) / delMax;

      if (varR === varMax) {
        h = delB - delG;
      } else if (varG === varMax) {
        h = 1 / 3 + delR - delB;
      } else if (varB === varMax) {
        h = 2 / 3 + delG - delR;
      }

      if (h < 0) {
        h += 1;
      }

      if (h > 1) {
        h -= 1;
      }
    }

    return new HLSImpl({ H: Math.floor(h * 360), L: Math.floor(s * 100), S: Math.floor(l * 100) });
  }

  public static fromXYZ(model: XYZ): HLS {
    const rgb = RGBImpl.fromXYZ(model);

    return this.fromRGB(rgb);
  }

  public static fromYUV(model: YUV): HLS {
    const rgb = RGBImpl.fromYUV(model);

    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: HLSModel) {
    super(colorRepresentation);
    this.typename = "HSL";
  }
}

export default HLSImpl;
