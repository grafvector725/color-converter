import AbstractColor from "./AbstractColor";
import type { CMYK, CMYKModel, HLS, HSB, LAB, RGB, XYZ, YUV } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";

/**
 * сине-зеденый,
 * Пурпурный,
 * Желтый,
 * Черный
 */
class CMYKImpl extends AbstractColor<CMYKModel> implements CMYK {
  public static fromRGB(model: RGB): CMYK {
    const { R, G, B } = model.getColorRepresentation();

    let C = 1 - R / 255;
    let M = 1 - G / 255;
    let Y = 1 - B / 255;

    let tmpK = 1;

    if (C < tmpK) {
      tmpK = C;
    }
    if (M < tmpK) {
      tmpK = M;
    }
    if (Y < tmpK) {
      tmpK = Y;
    }
    if (tmpK === 1) {
      C = 0;
      M = 0;
      Y = 0;
    } else {
      C = Math.round(((C - tmpK) / (1 - tmpK)) * 100);
      M = Math.round(((M - tmpK) / (1 - tmpK)) * 100);
      Y = Math.round(((Y - tmpK) / (1 - tmpK)) * 100);
    }
    const K = Math.round(tmpK * 100);

    return new CMYKImpl({ C, M, Y, K });
  }

  public static fromXYZ(model: XYZ): CMYK {
    const rgb = RGBImpl.fromXYZ(model);
    return this.fromRGB(rgb);
  }

  public static fromHLS(model: HLS): CMYK {
    const rgb = RGBImpl.fromHLS(model);
    return this.fromRGB(rgb);
  }

  public static fromLab(model: LAB): CMYK {
    const rgb = RGBImpl.fromLab(model);
    return this.fromRGB(rgb);
  }

  public static fromYUV(model: YUV): CMYK {
    const rgb = RGBImpl.fromYUV(model);

    return this.fromRGB(rgb);
  }

  public static fromHSB(model: HSB): CMYK {
    const rgb = RGBImpl.fromHSB(model);

    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: CMYKModel) {
    super(colorRepresentation);
    this.typename = "CMYK";
  }
}

export default CMYKImpl;
