import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HSB, LAB, RGB, XYZ, XYZModel, YUV } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";

class XYZImpl extends AbstractColor<XYZModel> implements XYZ {
  public static fromCMYK(model: CMYK): XYZ {
    const rgb = RGBImpl.fromCMYK(model);
    return this.fromRGB(rgb);
  }

  public static fromHLS(model: HLS): XYZ {
    const rgb = RGBImpl.fromHLS(model);
    return this.fromRGB(rgb);
  }

  public static fromHSB(model: HSB): XYZ {
    const rgb = RGBImpl.fromHSB(model);
    return this.fromRGB(rgb);
  }

  public static fromLab(model: LAB): XYZ {
    const { L, A, B } = model.getColorRepresentation();

    /**
     * Константы взяты из источника https://en.wikipedia.org/wiki/Lab_color_space
     */
    const refX = 95.047;
    const refY = 100.0;
    const refZ = 108.883;

    let varY = (L + 16) / 116;
    let varX = A / 500 + varY;
    let varZ = varY - B / 200;

    if (Math.pow(varY, 3) > 0.008856) {
      varY = Math.pow(varY, 3);
    } else {
      varY = (varY - 16 / 116) / 7.787;
    }

    if (Math.pow(varX, 3) > 0.008856) {
      varX = Math.pow(varX, 3);
    } else {
      varX = (varX - 16 / 116) / 7.787;
    }

    if (Math.pow(varZ, 3) > 0.008856) {
      varZ = Math.pow(varZ, 3);
    } else {
      varZ = (varZ - 16 / 116) / 7.787;
    }

    const x = Math.floor(varX * refX);
    const y = Math.floor(varY * refY);
    const z = Math.floor(varZ * refZ);

    return new XYZImpl({ X: x, Y: y, Z: z });
  }

  public static fromRGB(model: RGB): XYZ {
    const { R, G, B } = model.getColorRepresentation();

    let varR = R / 255;
    let varG = G / 255;
    let varB = B / 255;

    if (varR > 0.04045) {
      varR = Math.pow((varR + 0.055) / 1.055, 2.4);
    } else {
      varR = varR / 12.92;
    }

    if (varG > 0.04045) {
      varG = Math.pow((varG + 0.055) / 1.055, 2.4);
    } else {
      varG = varG / 12.92;
    }

    if (varB > 0.04045) {
      varB = Math.pow((varB + 0.055) / 1.055, 2.4);
    } else {
      varB = varB / 12.92;
    }

    varR = varR * 100;
    varG = varG * 100;
    varB = varB * 100;

    const X = Math.floor(varR * 0.4124 + varG * 0.3576 + varB * 0.1805);
    const Y = Math.floor(varR * 0.2126 + varG * 0.7152 + varB * 0.0722);
    const Z = Math.floor(varR * 0.0193 + varG * 0.1192 + varB * 0.9505);

    return new XYZImpl({ X, Y, Z });
  }

  public static fromYUV(model: YUV): XYZ {
    const rgb = RGBImpl.fromYUV(model);
    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: XYZModel) {
    super(colorRepresentation);
    this.typename = "XYZ";
  }
}

export default XYZImpl;
