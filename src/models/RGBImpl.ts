import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HSB, LAB, RGB, RGBModel, XYZ, YUV } from "./ColorModelsTypes";
import XYZImpl from "./XYZImpl";

class RGBImpl extends AbstractColor<RGBModel> implements RGB {
  private static clamp(n: number, low: number, high: number) {
    if (n < low) {
      return low;
    }
    if (n > high) {
      return high;
    }

    return n;
  }

  private static HueToRGB(v1: number, v2: number, vH: number): number {
    if (vH < 0) {
      vH += 1;
    }

    if (vH > 1) {
      vH -= 1;
    }

    if (6 * vH < 1) {
      return v1 + (v2 - v1) * 6 * vH;
    }

    if (2 * vH < 1) {
      return v2;
    }

    if (3 * vH < 2) {
      return v1 + (v2 - v1) * (2 / 3 - vH) * 6;
    }

    return v1;
  }

  public static fromHEX(hex: string): RGB {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    return result
      ? new RGBImpl({
          R: parseInt(result[1], 16),
          G: parseInt(result[2], 16),
          B: parseInt(result[3], 16),
        })
      : null;
  }

  public static fromCMYK(model: CMYK): RGB {
    let { C, M, Y, K } = model.getColorRepresentation();
    C = C / 100;
    M = M / 100;
    Y = Y / 100;
    K = K / 100;

    C = C * (1 - K) + K;
    M = M * (1 - K) + K;
    Y = Y * (1 - K) + K;

    const R = (1 - C) * 255;
    const G = (1 - M) * 255;
    const B = (1 - Y) * 255;

    return new RGBImpl({ R, G, B });
  }

  /**
   * https://ru.wikipedia.org/wiki/HSL
   */
  public static fromHLS(model: HLS): RGB {
    let { H, L, S } = model.getColorRepresentation();
    let R: number;
    let G: number;
    let B: number;

    if (S === 0) {
      R = L * 255;
      G = L * 255;
      B = L * 255;
    } else {
      let Q = 0;
      let P = 0;

      if (L < 0.5) {
        Q = L * (1 + S);
      } else {
        Q = L + S - S * L;
      }

      P = 2 * L - Q;

      R = Math.floor(255 * this.HueToRGB(P, Q, H + 1 / 3));
      G = Math.floor(255 * this.HueToRGB(P, Q, H));
      B = Math.floor(255 * this.HueToRGB(P, Q, H - 1 / 3));
    }

    return new RGBImpl({ R, B, G });
  }

  public static fromLab(model: LAB): RGB {
    /**
     * Преобразование идет как LAB -> XYZ -> RGB
     */
    const XYZModel = XYZImpl.fromLab(model);
    return this.fromXYZ(XYZModel);
  }

  public static fromXYZ(model: XYZ): RGB {
    let { X, Y, Z } = model.getColorRepresentation();

    X = X / 100;
    Y = Y / 100;
    Z = Z / 100;

    /**
     * Константы взяты отсюда https://en.wikipedia.org/wiki/SRGB#The_sRGB_transfer_function_.28.22gamma.22.29
     */

    let R = X * 3.2406 + Y * -1.5372 + Z * -0.4986;
    let G = X * -0.9689 + Y * 1.8758 + Z * 0.0415;
    let B = X * 0.0557 + Y * -0.204 + Z * 1.057;

    if (R > 0.0031308) {
      R = 1.055 * Math.pow(R, 1 / 2.4) - 0.055;
    } else {
      R = 12.92 * R;
    }

    if (G > 0.0031308) {
      G = 1.055 * Math.pow(G, 1 / 2.4) - 0.055;
    } else {
      G = 12.92 * G;
    }

    if (B > 0.0031308) {
      B = 1.055 * Math.pow(B, 1 / 2.4) - 0.055;
    } else {
      G = 12.92 * B;
    }

    return new RGBImpl({ R: Math.round(R * 255), G: Math.round(G * 255), B: Math.round(B * 255) });
  }

  /**
   * https://ru.wikipedia.org/wiki/YUV
   */
  public static fromYUV(model: YUV): RGB {
    let { Y, U, V } = model.getColorRepresentation();
    let R: number = 0;
    let G: number;
    let B: number;

    const kr = 0.2627;
    const kb = 0.0593;
    //
    // U = (0.5 * (255 - Y)) / (1 - kb);
    // V = (0.5 * (255 - Y)) / (1 - kr);
    //
    R = Math.floor(RGBImpl.clamp(Y + 1.402 * (V - 128), 0, 255));
    G = Math.floor(
      RGBImpl.clamp(Y - (kb * 1.772 * (U - 128) + kr * 1.402 * (V - 128)) / 0.587, 0, 255)
    );
    B = Math.floor(RGBImpl.clamp(Y + 1.772 * (U - 128), 0, 255));

    // R = Y + 1.4075 * (V - 128);
    // G = Y - 0.3455 * (U - 128) - 0.7169 * (V - 128);
    // B = Y + 1.779 * (U - 128);

    return new RGBImpl({ R, G, B });
  }

  public static fromHSB(model: HSB): RGB {
    let { H, B: V, S } = model.getColorRepresentation();

    let R,
      G,
      B = 0;

    if (S === 0) {
      R = V * 255;
      G = V * 255;
      B = V * 255;
    } else {
      H = H * 6;
      if (H === 6) {
        H = 0;
      }

      const H_i = Math.floor(H);
      const V_min = V * (1 - S);
      const V_dec = V * (1 - S * (H - H_i));
      const V_inc = V * (1 - S * (1 - (H - H_i)));

      switch (H_i) {
        case 0:
          R = V;
          G = V_inc;
          B = V_min;
          break;
        case 1:
          R = V_dec;
          G = V;
          B = V_min;
          break;
        case 2:
          R = V_min;
          G = V;
          B = V_inc;
          break;
        case 3:
          R = V_min;
          G = V_dec;
          B = V;
          break;
        case 4:
          R = V_inc;
          G = V_min;
          B = V;
          break;
        default:
          R = V;
          G = V_min;
          B = V_dec;
          break;
      }

      R = Math.floor(R * 255);
      G = Math.floor(G * 255);
      B = Math.floor(B * 255);
    }

    return new RGBImpl({ R, G, B });
  }

  constructor(colorRepresentation?: RGBModel) {
    super(colorRepresentation);
    this.typename = "RGB";
  }
}

export default RGBImpl;
