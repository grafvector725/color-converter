import { forEach } from "lodash";

export type TColorRepresentation = {
  [k: string]: number;
};

export interface IAbstractColor<T extends TColorRepresentation = TColorRepresentation> {
  getColorRepresentation(): T;
  setColorRepresentation(color: T): void;
  getTypename(): string;
  getColorCaption(): string;
}

abstract class AbstractColor<T extends TColorRepresentation = TColorRepresentation>
  implements IAbstractColor<T> {
  protected colorRepresentation: T = null;
  protected typename: string = "unknown";

  protected constructor(color?: T) {
    this.colorRepresentation = color;
  }

  public getColorRepresentation(): T {
    const color = this.colorRepresentation;
    const roundedColor: TColorRepresentation = {};

    forEach(color, (value, key) => {
      roundedColor[key] = Math.round(value);
    });

    return roundedColor as T;
  }

  public setColorRepresentation(color: T): void {
    this.colorRepresentation = color;
  }

  public getTypename(): string {
    return this.typename;
  }

  public getColorCaption(): string {
    const colorKeys = this.getColorRepresentation();
    let caption = "";

    forEach(colorKeys, (key) => {
      caption += `, ${key}`;
    });

    return caption.substring(1);
  }
}

export default AbstractColor;
