import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HSB, LAB, LABModel, RGB, XYZ, YUV } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";
import XYZImpl from "./XYZImpl";

class LABImpl extends AbstractColor<LABModel> implements LAB {
  public static fromCMYK(model: CMYK): LAB {
    const rgb = RGBImpl.fromCMYK(model);

    return this.fromRGB(rgb);
  }

  public static fromHLS(model: HLS): LAB {
    const rgb = RGBImpl.fromHLS(model);

    return this.fromRGB(rgb);
  }

  public static fromHSB(model: HSB): LAB {
    const xyz = XYZImpl.fromHSB(model);
    return this.fromXYZ(xyz);
  }

  public static fromRGB(model: RGB): LAB {
    const xyz = XYZImpl.fromRGB(model);
    return this.fromXYZ(xyz);
  }

  public static fromXYZ(model: XYZ): LAB {
    const { X, Y, Z } = model.getColorRepresentation();

    /**
     * Константы взяты из источника https://en.wikipedia.org/wiki/Lab_color_space
     */
    const refX = 95.047;
    const refY = 100.0;
    const refZ = 108.883;

    let varX = X / refX;
    let varY = Y / refY;
    let varZ = Z / refZ;

    if (varX > 0.008856) {
      varX = Math.pow(varX, 1 / 3);
    } else {
      varX = 7.787 * varX + 16 / 116;
    }

    if (varY > 0.008856) {
      varY = Math.pow(varY, 1 / 3);
    } else {
      varY = 7.787 * varY + 16 / 116;
    }

    if (varZ > 0.008856) {
      varZ = Math.pow(varZ, 1 / 3);
    } else {
      varZ = 7.787 * varZ + 16 / 116;
    }

    const L = 116 * varY - 16;
    const A = 500 * (varX - varY);
    const B = 200 * (varY - varZ);

    return new LABImpl({ L, A, B });
  }

  public static fromYUV(model: YUV): LAB {
    const rgb = RGBImpl.fromYUV(model);

    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: LABModel) {
    super(colorRepresentation);
    this.typename = "LAB";
  }
}

export default LABImpl;
