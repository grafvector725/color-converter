import AbstractColor from "./AbstractColor";
import type { CMYK, HLS, HSB, LAB, RGB, XYZ, YUV, YUVModel } from "./ColorModelsTypes";
import RGBImpl from "./RGBImpl";

class YUVImpl extends AbstractColor<YUVModel> implements YUV {
  public static fromCMYK(model: CMYK): YUV {
    const rgb = RGBImpl.fromCMYK(model);
    return this.fromRGB(rgb);
  }

  public static fromHLS(model: HLS): YUV {
    const rgb = RGBImpl.fromHLS(model);
    return this.fromRGB(rgb);
  }

  public static fromHSB(model: HSB): YUV {
    const rgb = RGBImpl.fromHSB(model);
    return this.fromRGB(rgb);
  }

  public static fromLab(model: LAB): YUV {
    const rgb = RGBImpl.fromLab(model);
    return this.fromRGB(rgb);
  }

  public static fromRGB(model: RGB): YUV {
    const { R, G, B } = model.getColorRepresentation();
    /**
     * Константы взяты из источника https://ru.wikipedia.org/wiki/YUV
     */
    const kR = 0.299;
    const kB = 0.114;

    const Y = kR * R + (1 - kR - kB) * G + kB * B;
    const U = (0.5 * (B - Y)) / (1 - kB) + 128;
    const V = (0.5 * (R - Y)) / (1 - kR) + 128;

    return new YUVImpl({ Y, U, V });
  }

  public static fromXYZ(model: XYZ): YUV {
    const rgb = RGBImpl.fromXYZ(model);
    return this.fromRGB(rgb);
  }

  constructor(colorRepresentation?: YUVModel) {
    super(colorRepresentation);
    this.typename = "YUV";
  }
}

export default YUVImpl;
