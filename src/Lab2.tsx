import React from "react";
import { Button, Col, Radio, Row, Slider } from "antd";
// import type { UploadFile } from "antd/lib/upload/interface";
import Polygon, { EInvertType, ENegativeType } from "./components/Polygon";
import type { RadioChangeEvent } from "antd/lib/radio";

interface ILab2Props {}
interface ILab2State {
  file: string;
  brightnessValue: number;
  contrastValue: number;
  negativeValue: number;
  invertType: EInvertType;
  negativeType: ENegativeType;
}

class Lab2 extends React.PureComponent<ILab2Props, ILab2State> {
  private fileFieldRef: React.RefObject<HTMLInputElement> = React.createRef();
  public readonly state: ILab2State = {
    file: undefined,
    brightnessValue: 0,
    contrastValue: 0,
    negativeValue: 0,
    invertType: null,
    negativeType: null,
  };

  constructor(props: ILab2Props) {
    super(props);
    this.handleSelectFile = this.handleSelectFile.bind(this);
    this.handleClickButton = this.handleClickButton.bind(this);
    this.handleChangeBrightness = this.handleChangeBrightness.bind(this);
    this.handleChangeNegative = this.handleChangeNegative.bind(this);
    this.handleChangeInvertType = this.handleChangeInvertType.bind(this);
    this.handleChangeNegativeType = this.handleChangeNegativeType.bind(this);
  }
  private handleSelectFile(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ file: URL.createObjectURL(event.target.files[0]) });
  }

  private handleClickButton(): void {
    this.fileFieldRef.current.click();
  }

  private handleChangeBrightness(value: number): void {
    this.setState({ brightnessValue: value });
  }

  private handleChangeNegative(value: number): void {
    this.setState({ negativeValue: value });
  }

  private handleChangeInvertType(event: RadioChangeEvent): void {
    const invertType: EInvertType = event.target.value;
    this.setState({ invertType });
  }

  private handleChangeNegativeType(event: RadioChangeEvent): void {
    const negativeType: ENegativeType = event.target.value;
    this.setState({ negativeType });
  }

  private renderImage(): React.ReactNode {
    const {
      file,
      brightnessValue,
      contrastValue,
      invertType,
      negativeType,
      negativeValue,
    } = this.state;

    if (file) {
      return (
        <Polygon
          file={this.state.file}
          brightnessValue={brightnessValue}
          contrastValue={contrastValue}
          invert={invertType}
          negative={negativeType}
          negativeValue={negativeValue}
        />
      );
    }
    return null;
  }

  public render(): React.ReactNode {
    return (
      <>
        <Row>
          <Col style={{ padding: 24 }}>
            <input
              ref={this.fileFieldRef}
              type={"file"}
              onChange={this.handleSelectFile}
              style={{ display: "none" }}
            />
            <Col>
              <Col>
                <Button onClick={this.handleClickButton} style={{ marginBottom: "16px" }}>
                  Выбрать изображение
                </Button>
              </Col>
              <Col>
                <Row>Инвертирование</Row>
                <Radio.Group onChange={this.handleChangeInvertType}>
                  <Row>
                    <Radio value={undefined}>Отсутствует</Radio>
                  </Row>
                  <Row>
                    <Radio value={EInvertType.FULL}>Полное</Radio>
                  </Row>
                  <Row>
                    <Radio value={EInvertType.HALF_LEFT_RIGHT}>Частичное(слева направо)</Radio>
                  </Row>
                  <Row>
                    <Radio value={EInvertType.HALF_UP_DOWN}>Частичное(сверху вниз)</Radio>
                  </Row>
                </Radio.Group>
                <Row>
                  <Col flex={"80px"}>
                    <span style={{ display: "inline-block", margin: "6px auto" }}>Яркость</span>
                  </Col>
                  <Col flex={"auto"}>
                    <Slider
                      key={"brightness"}
                      defaultValue={this.state.brightnessValue}
                      max={255}
                      min={-255}
                      onAfterChange={this.handleChangeBrightness}
                    />
                  </Col>
                </Row>

                <Row>
                  <Col flex={"80px"}>
                    <span style={{ display: "inline-block", margin: "6px auto" }}>Контраст</span>
                  </Col>
                  <Col flex={"auto"}>
                    <Slider
                      key={"contrast"}
                      defaultValue={this.state.contrastValue}
                      onAfterChange={this.handleChangeBrightness}
                      max={255}
                      min={-255}
                    />
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row>Негатив</Row>
                <Radio.Group onChange={this.handleChangeNegativeType}>
                  <Row>
                    <Radio value={undefined}>Отсутствует</Radio>
                  </Row>
                  <Row>
                    <Radio value={ENegativeType.FULL}>Полное</Radio>
                  </Row>
                  <Row>
                    <Radio value={ENegativeType.HALF_LEFT_RIGHT}>Частичное(слева направо)</Radio>
                  </Row>
                  <Row>
                    <Radio value={ENegativeType.HALF_UP_DOWN}>Частичное(сверху вниз)</Radio>
                  </Row>
                </Radio.Group>
                <Row>
                  <Col flex={"auto"}>
                    <Slider
                      key={"negative"}
                      defaultValue={this.state.negativeValue}
                      max={255}
                      min={0}
                      onAfterChange={this.handleChangeNegative}
                    />
                  </Col>
                </Row>
              </Col>
            </Col>
          </Col>
        </Row>
        <Row>
          <Col xs={{ span: 11, offset: 8 }} lg={{ span: 6, offset: 8 }}>
            {this.renderImage()}
          </Col>
        </Row>
      </>
    );
  }
}

export default Lab2;
