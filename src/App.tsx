import React from "react";
import "./App.css";
import RGBImpl from "./models/RGBImpl";
import CMYKImpl from "./models/CMYKImpl";
import LABImpl from "./models/LABImpl";
import XYZImpl from "./models/XYZImpl";
import HSBImpl from "./models/HSBImpl";
import HLSImpl from "./models/HLSImpl";
import YUVImpl from "./models/YUVImpl";
import type { IAbstractColor } from "./models/AbstractColor";
import type {
  RGBModel,
  CMYKModel,
  LABModel,
  YUVModel,
  HLSModel,
  HSBModel,
  XYZModel,
} from "./models/ColorModelsTypes";
import { map } from "lodash";

interface IAppProps {}
interface IAppState {
  hex: string;
}

class App extends React.PureComponent<IAppProps, IAppState> {
  private rgbModel: IAbstractColor<RGBModel> = null;
  private cmykModel: IAbstractColor<CMYKModel> = null;
  private labModel: IAbstractColor<LABModel> = null;
  private xyzModel: IAbstractColor<XYZModel> = null;
  private hsbModel: IAbstractColor<HSBModel> = null;
  private hlsModel: IAbstractColor<HLSModel> = null;
  private yovModel: IAbstractColor<YUVModel> = null;
  private colorModelList: IAbstractColor[] = [];

  public readonly state: IAppState = {
    hex: "#FFFFFF",
  };

  constructor(props: IAppProps) {
    super(props);

    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.updateColorSet();
  }

  public componentDidUpdate(prevProps: Readonly<IAppProps>, prevState: Readonly<IAppState>) {
    const { hex } = this.state;

    if (hex !== prevState.hex) {
      this.updateColorSet();
    }
  }

  private updateColorSet(): void {
    const { hex } = this.state;
    if (hex) {
      const rgb = RGBImpl.fromHEX(hex);
      const cmyk = CMYKImpl.fromRGB(rgb);
      const lab = LABImpl.fromRGB(rgb);
      const xyz = XYZImpl.fromRGB(rgb);
      const yuv = YUVImpl.fromRGB(rgb);
      const hls = HLSImpl.fromRGB(rgb);
      const hsb = HSBImpl.fromRGB(rgb);

      this.rgbModel = rgb;
      this.labModel = lab;
      this.yovModel = yuv;
      this.xyzModel = xyz;
      this.cmykModel = cmyk;
      this.hlsModel = hls;
      this.hsbModel = hsb;

      this.colorModelList = [
        this.rgbModel,
        this.cmykModel,
        this.hsbModel,
        this.hlsModel,
        this.xyzModel,
        this.yovModel,
        this.labModel,
      ];
    }
  }

  private static getRow(model: IAbstractColor): React.ReactNode {
    return (
      <tr key={model.getTypename()} className={"row"}>
        <td className={"cell typename"}>{model?.getTypename?.()}</td>
        <td className={"cell color"}>{model?.getColorCaption?.()}</td>
      </tr>
    );
  }

  private getTable(): React.ReactNode {
    return (
      <div className={"table"}>
        <table>
          <tbody>{map(this.colorModelList, (model) => App.getRow(model))}</tbody>
        </table>{" "}
      </div>
    );
  }

  private handleChangeColor(e: React.ChangeEvent<HTMLInputElement>): void {
    const hex = e.target?.value;

    if (this.state.hex !== hex) {
      this.setState({ hex });
    }
  }

  public render(): React.ReactNode {
    return (
      <div className={"wrapper"}>
        <div className={"color-select"}>
          <input
            type={"color"}
            onChange={this.handleChangeColor}
            className={"picker"}
            value={this.state.hex}
          />
        </div>
        {this.getTable()}
      </div>
    );
  }
}

export default App;
